﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class DecimalToBinary
    {
        static public void Opgave()
        {

            Program.current = () => Opgave();
            Console.Clear();
            Console.WriteLine("Opgave Decimal til Binær");
            DecimalToBinary BinaryConverter = new DecimalToBinary();
            
            int input = (int)Method.AskUserForInput("Indtast heltal som konverteres til binær: ",InputTypes.Int32);

            string binaryValue = BinaryConverter.ToBinary(input);

            Console.WriteLine("Dit tal i binær er: {0}",binaryValue);
            Console.ReadLine();
            Program.Menu();
        }

        public string ToBinary(int input)
        {
            string container = "";
            int rem;
            while (input > 0)
            {
                rem = input % 2;
                input = input / 2;
                container = (Convert.ToString(rem))+container;
            }

            return container;
        }

    }
}