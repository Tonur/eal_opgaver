﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave2
    {
        static void Opgave2_6()
        {
            Program.current = () => Opgave2_6();
            Console.Clear();
            Console.WriteLine("Opgave 2,6");

            var cpr = (double)Method.AskUserForInput("Indtast de første 9 cifre i dit CPR Nr.: ", InputTypes.Double);

            int c10;
            int c9 = ((int)cpr / 1 % 10);
            int c8 = ((int)cpr / 10 % 10);
            int c7 = ((int)cpr / 100 % 10);
            int c6 = ((int)cpr / 1000 % 10);
            int c5 = ((int)cpr / 10000 % 10);
            int c4 = ((int)cpr / 100000 % 10);
            int c3 = ((int)cpr / 1000000 % 10);
            int c2 = ((int)cpr / 10000000 % 10);
            int c1 = ((int)cpr / 100000000 % 10);

            //Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}", c1, c2, c3, c4, c5, c6, c7, c8, c9);                
            //Console.ReadLine();

            c1 = c1 * 4;
            c2 = c2 * 3;
            c3 = c3 * 2;
            c4 = c4 * 7;
            c5 = c5 * 6;
            c6 = c6 * 5;
            c7 = c7 * 4;
            c8 = c8 * 3;
            c9 = c9 * 2;
            int sum = c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8 + c9;
            //Console.WriteLine("Summen er: {0}", sum1);            

            if (sum % 11 == 0)
            {
                c10 = 0;
                Console.WriteLine("Sidste cifre er: {0}", c10);
                Console.ReadLine();
                Intro();
            }
            else
            {
                c10 = 11 - (sum % 11);
                Console.WriteLine("Sidste cifre er: {0}", c10);
                Console.ReadLine();
                Intro();
            }
        }
    }    
}