﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave7
    {
        static void Opgave7_5()
        {

            Program.current = () => Opgave7_5();
            Console.Clear();
            Console.WriteLine("Opgave 7,5");

            for (int i = 65; i < 91; i++)
            {
                Console.WriteLine("U+{0:x}",i);
            }
            Console.ReadLine();

            Intro();
        }

    }
}