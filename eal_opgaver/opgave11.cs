﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public class Opgave11
    {
        static bool errorCode = true;       

        public static void Opgave()
        {

            Intro();
        }
            static void Error()
        {
            if (!errorCode)
            {
                Console.WriteLine("Ugyldtig værdi indtastet. Prøv igen.");
                Console.ReadLine();
                Program.current();
            }
        }

        static void Intro()
        {
            Opgave11 enOpgave = new Opgave11();
            Console.Clear();
            Console.WriteLine("Intro");
            Console.WriteLine("Hvilken opgave vil du gå til?");
            Console.WriteLine("1: Opgave 10,1 \t Metodekald med inputs");
            Console.WriteLine("2: Opgave 10,2 \t To be or not to be - Statisk");
            Console.WriteLine("3: Opgave 10,3 \t Metode om skudår");
            Console.WriteLine("4: Tilbage til menu");
            Console.WriteLine("Indtast nummer...");

            ConsoleKeyInfo keyPress = Console.ReadKey();

            if (keyPress.Key == ConsoleKey.NumPad1 || keyPress.Key == ConsoleKey.D1)
            {
                enOpgave.Opgave11_1();
            }
            else if (keyPress.Key == ConsoleKey.NumPad2 || keyPress.Key == ConsoleKey.D2)
            {
                enOpgave.Opgave11_2();
            }
            else if (keyPress.Key == ConsoleKey.NumPad3 || keyPress.Key == ConsoleKey.D3)
            {
                enOpgave.Opgave11_3();
            }
            
            else if (keyPress.Key == ConsoleKey.NumPad4 || keyPress.Key == ConsoleKey.D4)
            {
                Program.Menu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Jeg forstod ikke hvad du trykkede. Prøv igen.");
                Console.ReadLine();
                Intro();
            }
        }
        void Opgave11_1()
        {
            Program.current = () => Opgave11_1();
            Console.Clear();
            Console.WriteLine("Opgave 11,1");

            int start = 5;
            int limit = 8;

            Opgave11 enOpgave11 = new Opgave11();

            int value = enOpgave11.Rutine(start, limit);

            Console.WriteLine("Slutresultat er: {0}", value);
            Console.ReadLine();
            Intro();
        }
        public int Rutine(int startFelt, int limit)
        {
            Console.WriteLine("Startfelt er pt.: {0}", startFelt);
            if (startFelt > limit)
                return -1;
            else if (startFelt == limit)
                return 1;
            else
            {
                int temp = startFelt * Rutine(startFelt + 1, limit);
                Console.WriteLine("Temp er pt.: {0}.", temp);
                return temp;
            }
        }
        void Opgave11_2()
        {
            Program.current = () => Opgave11_2();
            Console.Clear();
            Console.WriteLine("Opgave 11,2");
            Opgave11 enOpgave11 = new Opgave11();


            int b = 1;
            int r = 4;
            int n = 5;

            double value = enOpgave11.Rutine(b, r, n);

            Console.WriteLine("Value er: {0}.", value);

            Console.ReadLine();
            Intro();
        }
        public double Rutine(int b, int r, int n)
        {
            Console.WriteLine("b er pt: {0}, r er pt: {1} og n er pt: {2}.", b, r, n);
            if (n == 0)
                return 0;
            else
            {
                double temp = (b + r) + Rutine(b, (r - 1), (n - 1));
                Console.WriteLine("r er pt.: {0} og b er pt.: {1}", r, b);
                Console.WriteLine("Midlertidigt tal er: {0}", temp);
                return temp;
            }
        }
        void Opgave11_3()
        {
            Program.current = () => Opgave11_3();
            Console.Clear();
            Console.WriteLine("Opgave 11,3");
            Opgave11 enOpgave11 = new Opgave11();

            int n = 87915;
            int value = enOpgave11.Rutine(n);

            Console.WriteLine("Resultatet er: {0}", value);

            Console.ReadLine();
            Intro();
        }
        public int Rutine(int n)
        {
            Console.WriteLine("n er pt.: {0}", n);
            if (n / 10 == 0)
                return n;
            else
            {
                int temp = Rutine(n / 10) + n % 10;
                Console.WriteLine("n er pt.: {0}", n);
                Console.WriteLine("Midlertidig er {0}", temp);
                return temp;
            }
        }
    }
}