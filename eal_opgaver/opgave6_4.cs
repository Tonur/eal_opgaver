﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave6
    {
        static void Opgave6_4()
        {

            Program.current = () => Opgave6_4();
            Console.Clear();
            Console.WriteLine("Opgave 6,4");

            Console.WriteLine("Man kan ikke compare variabler og tal i en case." +
                "En case er for det meste til enten input eller bestemte variabler som man kender værdien af.");
            Console.ReadLine();

            Intro();
        }        
    }    
}