﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave10
    {
               
        static void Opgave10_4()
        {
            Program.current = () => Opgave10_4();
            Console.Clear();
            Console.WriteLine("Opgave 10,4");

            Console.WriteLine("Dato Operation");
            var date = (int)Method.AskUserForInput("Indtast din dato i format 'YYYYMMDD': ", InputTypes.Int32);

            bool valid = Valid(date);

            if (valid)
            {
                Console.WriteLine("Datoen er gyldig.");
            }
            else if (!valid)
            {
                Console.WriteLine("Ugyldig dato, prøv igen...");
                Opgave10_4();
            }

            Console.ReadLine();
            Intro();
        }

        static bool Valid(int date)
        {
            bool check = false;

            
            var year = ((int)date / 10000);
            var month = ((int)date / 100) % 100;
            var day = (int)date % 100;
            int dayMax = 0;
            
            
            //Leapyear Check

            if (DateTime.IsLeapYear(year))
            {
                dayMax = 29;
            }
            if (!DateTime.IsLeapYear(year))
            {
                dayMax = 28;
            }
            if (month == 4 || month == 6 || month == 9 || month == 11)
            {
                dayMax = 30;
            }
            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
            {
                dayMax = 31;
            }

            if (month > 12 || day > dayMax)
            {
                Console.WriteLine("Du har indtastet en ugyldig dato. Prøv igen.");
                Console.ReadLine();
                Opgave10_4();
            }
            else
            {
                check = true;
            }

            return check;
        }
    }    
}