﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave4
    {
        static void Opgave4_5()
        {

            Program.current = () => Opgave4_5();
            Console.Clear();
            Console.WriteLine("Opgave 4,5");
            int year = (int)Method.AskUserForInput
                ("Indtast et år i format 'YYYY' for at teste for skudår: ",
                InputTypes.Int32);

            if (DateTime.IsLeapYear(year))
            {
                Console.WriteLine("Hurah! Det er et skudår!");
                Console.ReadLine();
                Intro();
            }
            if (!DateTime.IsLeapYear(year))
            {
                Console.WriteLine("Det er descærre ikke et skudår...");
                Console.ReadLine();
                Intro();
            }

        }
    }    
}