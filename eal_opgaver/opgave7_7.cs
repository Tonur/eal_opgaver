﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave7
    {
        static void Opgave7_7()
        {
            Program.current = () => Opgave7_7();
            Console.Clear();
            Console.WriteLine("Opgave 7,7");

            string tekst = "0123456789ÆØÅæøå";
            foreach (char ch in tekst) Console.WriteLine("U+{0:x}",(int)ch);

            Console.ReadLine();

            Intro();
        }

    }
}