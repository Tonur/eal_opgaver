﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave5
    {

        public static int x;

        public static void Opgave()
        {
            Console.Clear();
            Console.WriteLine("Opgave 5");
            Console.WriteLine("Sammenligne strings.");
            Program.current = () => Opgave();

            string string1;
            string string2;

            string1 = "elefant";
            string2 = "Elefant";
            x = string1.CompareTo(string2);
            Console.WriteLine("Streng 1 er {0}, Streng 2 er {1}. Vi sammenligner dem og x bliver derefter {2}", string1, string2, x);
                                                             
            string1 = "2230";                                
            string2 = "8";                                   
            x = string1.CompareTo(string2);                  
            Console.WriteLine("Streng 1 er {0}, Streng 2 er {1}. Vi sammenligner dem og x bliver derefter {2}", string1, string2, x);
                                                             
                                                             
            string1 = "Tom";                                 
            string2 = "Tim";                                 
            x = string1.CompareTo(string2);                  
            Console.WriteLine("Streng 1 er {0}, Streng 2 er {1}. Vi sammenligner dem og x bliver derefter {2}", string1, string2, x);
                                                             
            string1 = "C#";                                  
            string2 = "Chip";                                
            x = string1.CompareTo(string2);                  
            Console.WriteLine("Streng 1 er {0}, Streng 2 er {1}. Vi sammenligner dem og x bliver derefter {2}", string1, string2, x);
                                                             
            string1 = "skål";                                
            string2 = "skæl";                                
            x = string1.CompareTo(string2);                  
            Console.WriteLine("Streng 1 er {0}, Streng 2 er {1}. Vi sammenligner dem og x bliver derefter {2}", string1, string2, x);
                                                             
            string1 = "111";                                
            string2 = "222";                               
            x = string1.CompareTo(string2);                  
            Console.WriteLine("Streng 1 er {0}, Streng 2 er {1}. Vi sammenligner dem og x bliver derefter {2}", string1, string2, x);
                                                             
            string1 = "monster";                             
            string2 = "monster";                                
            x = string1.CompareTo(string2);                  
            Console.WriteLine("Streng 1 er {0}, Streng 2 er {1}. Vi sammenligner dem og x bliver derefter {2}", string1, string2, x);

            Console.ReadLine();
            Program.Menu();
        }
    }
}