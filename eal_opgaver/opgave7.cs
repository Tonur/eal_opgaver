﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave7
    {
        static bool errorCode = true;       

        public static void Opgave()
        {

            Intro();
        }
            static void Error()
        {
            if (!errorCode)
            {
                Console.WriteLine("Ugyldtig værdi indtastet. Prøv igen.");
                Console.ReadLine();
                Program.current();
            }
        }

        static void Intro()
        {
            Console.Clear();
            Console.WriteLine("Intro");
            Console.WriteLine("Hvilken opgave vil du gå til?");
            Console.WriteLine("1: Opgave 7,1 \t Kvadrattal");
            Console.WriteLine("2: Opgave 7,2 \t Gennemsnit");
            Console.WriteLine("3: Opgave 7,3 \t Lige/Ulige tal");
            Console.WriteLine("4: Opgave 7,4 \t Forskel 3 tal");
            Console.WriteLine("5: Opgave 7,5 \t Unicodes");
            Console.WriteLine("6: Opgave 7,6 \t Tabel");
            Console.WriteLine("6: Opgave 7,7 \t Forskning");
            Console.WriteLine("7: Tilbage til menu");
            Console.WriteLine("Indtast nummer...");

            ConsoleKeyInfo keyPress = Console.ReadKey();

            if (keyPress.Key == ConsoleKey.NumPad1 || keyPress.Key == ConsoleKey.D1)
            {
                Opgave7_1();
            }
            else if (keyPress.Key == ConsoleKey.NumPad2 || keyPress.Key == ConsoleKey.D2)
            {
                Opgave7_2();
            }
            else if (keyPress.Key == ConsoleKey.NumPad3 || keyPress.Key == ConsoleKey.D3)
            {
                Opgave7_3();
            }
            else if (keyPress.Key == ConsoleKey.NumPad4 || keyPress.Key == ConsoleKey.D4)
            {
                Opgave7_4();
            }
            else if (keyPress.Key == ConsoleKey.NumPad5 || keyPress.Key == ConsoleKey.D5)
            {
                Opgave7_5();
            }
            else if (keyPress.Key == ConsoleKey.NumPad6 || keyPress.Key == ConsoleKey.D6)
            {
                Opgave7_6();
            }
            else if (keyPress.Key == ConsoleKey.NumPad7 || keyPress.Key == ConsoleKey.D7)
            {
                Opgave7_7();
            }
            else if (keyPress.Key == ConsoleKey.NumPad8 || keyPress.Key == ConsoleKey.D8)
            {
                Program.Menu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Jeg forstod ikke hvad du trykkede. Prøv igen.");
                Console.ReadLine();
                Intro();
            }
        }
    }
}