﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave7
    {
        static void Opgave7_3()
        {

            Program.current = () => Opgave7_3();
            Console.Clear();
            Console.WriteLine("Opgave 7,3");

            for (int i = 0; i < 26; i++)
            {
                if(i%2==0)
                Console.WriteLine(i);
            }
            for (int i = -25; i < 0; i++)
            {
                if(i%2!=0)
                Console.WriteLine(i);
            }
            
            Console.ReadLine();
            Intro();
        }

    }
}