﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave10
    {
               
        static void Opgave10_5()
        {
            Program.current = () => Opgave10_5();
            Console.Clear();
            Console.WriteLine("Opgave 10,5: ");

            Console.WriteLine("Indtast en streng og check hvor mange a'er der er i.");
            string userInput = Console.ReadLine();

            int letterNumber = CheckYoA(userInput);
            Console.WriteLine("Der er {0} a'er i din tekst.",letterNumber);
            
            Console.ReadLine();
            Intro();
        }

        private static int CheckYoA(string userInput)
        {
            int count = 0;

            foreach(char a in userInput)
            {
                count++;
            }
            return count;

        }
    }    
}