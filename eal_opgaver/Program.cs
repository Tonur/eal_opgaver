﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;

namespace eal_opgaver
{
    class Program
    {
        public static Action current = null;
        public static System.Text.Encoding UTF8 { get; }

        public static void Main(string[] args)
        {
            //Forslag: Kald den ShowMenu
            Menu();
        }
        static public void Menu()
        {
            Console.Clear();
            Console.WriteLine("Menu");
            Console.WriteLine("Hvilke opgave vil du gå til?");
            Console.WriteLine("1: Opgave 1-10 Intro - Konvertering, if, loops og metoder");
            Console.WriteLine("2: Opgave 11-20 Grundlæggende - Rekursion og arrays");
            Console.WriteLine("0: Opgave Special - Typekonvertering osv.");


            ConsoleKeyInfo keyPress = Console.ReadKey();

            if (keyPress.Key == ConsoleKey.NumPad1 || keyPress.Key == ConsoleKey.D1)
            {
                Opgave1to10();
            }
            else if (keyPress.Key == ConsoleKey.NumPad2 || keyPress.Key == ConsoleKey.D2)
            {
                Opgave11to20();
            }
            else if (keyPress.Key == ConsoleKey.NumPad0 || keyPress.Key == ConsoleKey.D0)
            {
                OpgaveSpecial();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Du trykkede på en ugyldig tast. Prøv igen.");
                Console.ReadLine();
                Menu();
            }
        }
        static public void Opgave1to10()
        {
            Console.WriteLine("1: Opgave 1 \t Intro, værdier og typer");
            Console.WriteLine("2: Opgave 2 \t Typearbejde, konvertering " +
                "og aritmiske operatorer + tal fra string");
            Console.WriteLine("3: Opgave 3 \t Andengradsligning");
            Console.WriteLine("4: Opgave 4 \t If statements");
            Console.WriteLine("5: Opgave 5 \t Sammenlign tekst");
            Console.WriteLine("6: Opgave 6 \t Advanceret if og switch");
            Console.WriteLine("7: Opgave 7 \t For loops");
            Console.WriteLine("8: Opgave 8 \t Loops Andengradsligning");
            Console.WriteLine("9: Opgave 9 \t Dyr opgave");
            Console.WriteLine("0: Opgave 10 \t Metoder");
            Console.WriteLine("Esc: Tilbage til menu");
            Console.WriteLine("Indtast nummer...");

            ConsoleKeyInfo keyPress = Console.ReadKey();

            if (keyPress.Key == ConsoleKey.NumPad1 || keyPress.Key == ConsoleKey.D1)
            {
                Opgave1.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad2 || keyPress.Key == ConsoleKey.D2)
            {
                Opgave2.Opgave();
            }            
            else if (keyPress.Key == ConsoleKey.NumPad3 || keyPress.Key == ConsoleKey.D3)
            {
                Opgave3.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad4 || keyPress.Key == ConsoleKey.D4)
            {
                Opgave4.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad5 || keyPress.Key == ConsoleKey.D5)
            {
                Opgave5.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad6 || keyPress.Key == ConsoleKey.D6)
            {
                Opgave6.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad7 || keyPress.Key == ConsoleKey.D7)
            {
                Opgave7.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad8 || keyPress.Key == ConsoleKey.D8)
            {
                Opgave8.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad9 || keyPress.Key == ConsoleKey.D9)
            {
                Opgave9.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad0 || keyPress.Key == ConsoleKey.D0)
            {
                Opgave10.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.Escape)
            {
                Menu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Du trykkede på en ugyldig tast. Prøv igen.");
                Console.ReadLine();
                Opgave1to10();
            }
        }
        static public void Opgave11to20()
        {
            Console.WriteLine("1: Opgave 11 \t Rekursion");
            Console.WriteLine("2: Opgave 12 \t Arrays");
            Console.WriteLine("3: Opgave 13 \t");
            Console.WriteLine("4: Opgave 14 \t");
            Console.WriteLine("5: Opgave 15 \t");
            Console.WriteLine("6: Opgave 16 \t");
            Console.WriteLine("7: Opgave 17 \t");
            Console.WriteLine("8: Opgave 18 \t");
            Console.WriteLine("9: Opgave 19 \t");
            Console.WriteLine("0: Opgave 20 \t");
            Console.WriteLine("Esc: Tilbage til menu");
            Console.WriteLine("Indtast nummer...");

            ConsoleKeyInfo keyPress = Console.ReadKey();

            if (keyPress.Key == ConsoleKey.NumPad1 || keyPress.Key == ConsoleKey.D1)
            {
                Opgave11.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad2 || keyPress.Key == ConsoleKey.D2)
            {
                Opgave12.Opgave();
            }
            /*
            else if (keyPress.Key == ConsoleKey.NumPad3 || keyPress.Key == ConsoleKey.D3)
            {
                Opgave13.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad4 || keyPress.Key == ConsoleKey.D4)
            {
                Opgave14.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad5 || keyPress.Key == ConsoleKey.D5)
            {
                Opgave15.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad6 || keyPress.Key == ConsoleKey.D6)
            {
                Opgave16.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad7 || keyPress.Key == ConsoleKey.D7)
            {
                Opgave17.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad8 || keyPress.Key == ConsoleKey.D8)
            {
                Opgave18.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad9 || keyPress.Key == ConsoleKey.D9)
            {
                Opgave19.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.NumPad0 || keyPress.Key == ConsoleKey.D0)
            {
                Opgave20.Opgave();
            }
            */
            else if (keyPress.Key == ConsoleKey.Escape)
            {
                Menu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Du trykkede på en ugyldig tast. Prøv igen.");
                Console.ReadLine();
                Opgave11to20();
            }
        }
        static public void OpgaveSpecial()
        {
            ConsoleKeyInfo keyPress = Console.ReadKey();

            Console.WriteLine("1: Opgave r \t Random AF");
            Console.WriteLine("2: Opgave f \t Factorial");
            Console.WriteLine("3: Opgave p \t Decimal Til Binær");
            Console.WriteLine("4: Opgave G \t Binær til Decimal");
            Console.WriteLine("5: Opgave G \t Tal Converter");           
            Console.WriteLine("Esc: Tilbage til menu");
            Console.WriteLine("Indtast nummer...");

            if (keyPress.Key == ConsoleKey.R)
            {
                randomNumber.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.B)
            {
                BruteForce.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.F)
            {
                Factoriality.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.P)
            {
                DecimalToBinary.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.G)
            {
                BinaryToDecimal.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.L)
            {
                NumberConverter.Opgave();
            }
            else if (keyPress.Key == ConsoleKey.Escape)
            {
                Menu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Du trykkede på en ugyldig tast. Prøv igen.");
                Console.ReadLine();
                OpgaveSpecial();
            }       
            
        }
    }
}
