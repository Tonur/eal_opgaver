﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave4
    {
        static bool errorCode = true;       

        public static void Opgave()
        {

            Intro();
        }
            static void Error()
        {
            if (!errorCode)
            {
                Console.WriteLine("Ugyldtig værdi indtastet. Prøv igen.");
                Console.ReadLine();
                Program.current();
            }
        }

        static void Intro()
        {
            Console.Clear();
            Console.WriteLine("Intro");
            Console.WriteLine("Hvilken opgave vil du gå til?");
            Console.WriteLine("1: Opgave 4,1 \t Store tal");
            Console.WriteLine("2: Opgave 4,2 \t Post brev");
            Console.WriteLine("3: Opgave 4,3 \t Enhedspris + moms");
            Console.WriteLine("4: Opgave 4,4 \t Ugeløn");
            Console.WriteLine("5: Opgave 4,5 \t Skudår");
            Console.WriteLine("6: Opgave 4,6 \t XOR");
            Console.WriteLine("7: Tilbage til menu");
            Console.WriteLine("Indtast nummer...");

            ConsoleKeyInfo keyPress = Console.ReadKey();

            if (keyPress.Key == ConsoleKey.NumPad1 || keyPress.Key == ConsoleKey.D1)
            {
                Opgave4_1();
            }
            else if (keyPress.Key == ConsoleKey.NumPad2 || keyPress.Key == ConsoleKey.D2)
            {
                Opgave4_2();
            }
            else if (keyPress.Key == ConsoleKey.NumPad3 || keyPress.Key == ConsoleKey.D3)
            {
                Opgave4_3();
            }
            else if (keyPress.Key == ConsoleKey.NumPad4 || keyPress.Key == ConsoleKey.D4)
            {
                Opgave4_4();
            }
            else if (keyPress.Key == ConsoleKey.NumPad5 || keyPress.Key == ConsoleKey.D5)
            {
                Opgave4_5();
            }
            else if (keyPress.Key == ConsoleKey.NumPad6 || keyPress.Key == ConsoleKey.D6)
            {
                Opgave4_6();
            }
            else if (keyPress.Key == ConsoleKey.NumPad7 || keyPress.Key == ConsoleKey.D7)
            {
                Program.Menu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Jeg forstod ikke hvad du trykkede. Prøv igen.");
                Console.ReadLine();
                Intro();
            }
        }
    }
}