﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave8
    {

        public static void Opgave()
        {

            Program.current = () => Opgave();
            Console.Clear();
            Console.WriteLine("Opgave 8");


            Console.WriteLine("Beregn andengradslignings lønsning.");
            Console.WriteLine("Formlen er: a\u00b2 * x + b * x + c.");
            double D;
            double x;
            double x1;
            double x2;
            double y;
            double y1;
            double y2;

            Console.WriteLine("");

            for (int a = 1; a <= 2; a++)
            {
                for (int b = 1; b <= 3; b++)
                {
                    for (int c = -5; c <= 4; c++)
                    {

                        D = Math.Pow(b, 2) - 4 * a * c;
                        Console.WriteLine("Formlen er: {0}\u00b2 * x + {1} * x + {2} " +
                            "og diskriminanten er: {3}.", a, b, c, D);

                        if (D < 0)
                        {
                            //Ingen løsning
                            Console.WriteLine("Der er ingen løsning tilgængelig til denne formel.");
                            Console.WriteLine();
                        }
                        if (D == 0)
                        {
                            //En løsning
                            x = (-b + Math.Sqrt(D)) / (2 * a);
                            y = (Math.Pow(a, 2) * x) + (b * x) + c;
                            Console.WriteLine("1 løsning: Punktet ligger på X: {0:n2} og Y: {1:n2}", x, y);
                            Console.WriteLine();
                        }
                        if (D > 0)
                        {
                            //To løsninger
                            x1 = (-b + Math.Sqrt(D)) / (2 * a);
                            x2 = (-b - Math.Sqrt(D)) / (2 * a);
                            y1 = (Math.Pow(a, 2) * x1) + (b * x1) + c;
                            y2 = (Math.Pow(a, 2) * x2) + (b * x2) + c;

                            Console.WriteLine("2 løsninger: Punkterne ligger på X1: {0:n2} Y1: {1:n2} " +
                                "og X2: {2:n2} Y2: {3:n2}.", x1, y1, x2, y2);
                            Console.WriteLine();
                        }
                    }
                }
            }
            Console.ReadLine();
            Program.Menu();
        }
    }
}
            