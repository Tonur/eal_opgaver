﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave2
    {

        static void Opgave2_4()
        {
            Program.current = () => Opgave2_4();
            Console.Clear();
            Console.WriteLine("Opgave 2,4");
            Console.WriteLine("Typekonvertering.");

            typeconvert();

            void typeconvert()
            {
                Console.WriteLine("Hvilken type vil du afprøve?");
                Console.WriteLine("1: Int \t 2: Double \t 3: Byte \t 4: Long \t 5: Afslut til Intro");

                ConsoleKeyInfo keyPress = Console.ReadKey();

                if (keyPress.Key == ConsoleKey.NumPad1 || keyPress.Key == ConsoleKey.D1)
                {
                    Console.WriteLine("1: Int");
                    var intNum = (int)Method.AskUserForInput("Indtast tal: ", InputTypes.Double);

                    Console.WriteLine("Dit tal " + intNum + " konverteres nu til double:");
                    var doubleNum = Convert.ToDouble(intNum);

                    Console.WriteLine("Tada! Dit tal står nu som double!");
                    Console.WriteLine("Dit tal: {0:n2}", doubleNum);
                    typeconvert();
                }
                else if (keyPress.Key == ConsoleKey.NumPad2 || keyPress.Key == ConsoleKey.D2)
                {
                    Console.WriteLine("2: Double");
                    var doubleNum = (double)Method.AskUserForInput("Indtast tal: ", InputTypes.Double);

                    Console.WriteLine("Dit tal " + doubleNum + " konverteres nu til int:");
                    var intNum = Convert.ToInt32(doubleNum);

                    Console.WriteLine("Tada! Dit tal står nu som int!");
                    Console.WriteLine("Dit tal: {0}", intNum);
                    typeconvert();
                }
                else if (keyPress.Key == ConsoleKey.NumPad3 || keyPress.Key == ConsoleKey.D3)
                {
                    Console.WriteLine("3: Byte");
                    var byteNum = (int)Method.AskUserForInput("Indtast tal: ", InputTypes.Double);

                    Console.WriteLine("Dit tal " + byteNum + " konverteres nu til int:");
                    var intNum = Convert.ToInt32(byteNum);

                    Console.WriteLine("Tada! Dit tal står nu som int!");
                    Console.WriteLine("Dit tal: {0}", intNum);
                    typeconvert();
                }
                else if (keyPress.Key == ConsoleKey.NumPad4 || keyPress.Key == ConsoleKey.D4)
                {
                    Console.WriteLine("4: Long");
                    var longNum = (long)Method.AskUserForInput("Indtast tal: ", InputTypes.Double);

                    Console.WriteLine("Dit tal " + longNum + " konverteres nu til int:");
                    var intNum = Convert.ToInt32(longNum);

                    Console.WriteLine("Tada! Dit tal står nu som int!");
                    Console.WriteLine("Dit tal: {0}", intNum);
                    typeconvert();
                }
                else if (keyPress.Key == ConsoleKey.NumPad5 || keyPress.Key == ConsoleKey.D5)
                {
                    Intro();
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Jeg forstod ikke hvad du trykkede. Prøv igen.");
                    Console.ReadLine();
                    typeconvert();
                }
            }
        }
    }    
}