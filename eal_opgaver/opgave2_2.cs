﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave2
    {

        static void Opgave2_2()
        {
            Program.current = () => Opgave2_2();
            Console.Clear();
            Console.WriteLine("Opgave 2,2");

            Console.WriteLine("Konverter Celsius til Fahrenheit.");
            var temperature = (double)Method.AskUserForInput("Indtast Celcius grader: ", InputTypes.Double);
            temperature = temperature * 1.80 + 32;
            Console.WriteLine("Fahrenheit temperatur: {0:n2}", temperature);
            Console.ReadLine();
            Intro();
        }
    }    
}