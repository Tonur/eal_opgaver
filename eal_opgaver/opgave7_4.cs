﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave7
    {
        static void Opgave7_4()
        {

            Program.current = () => Opgave7_4();
            Console.Clear();
            Console.WriteLine("Opgave 7,4");
            Console.WriteLine();
            for (int i = 302; i <= 398; i++)
            {
                int i3 = i/100;
                int i2 = (i / 10) % 10;
                int i1 = i % 10;
                if (i1 != i2 && i1 != i3 && i2 !=i3)
                {
                    Console.Write("{0}\t",i);
                }

            }
            Console.ReadLine();

            Intro();
        }

    }
}