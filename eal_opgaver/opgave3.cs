﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave3
    {

        public static void Opgave()
        {
            Program.current = () => Opgave();
            Console.Clear();
            Console.WriteLine("Opgave 3");
            Console.WriteLine("Beregn andengradslignings lønsning.");
            Console.WriteLine("Formlen er: a\u00b2 * x + b * x + c.");

            double a = (Double)Method.AskUserForInput("Indtast a: ", InputTypes.Double);
            double b = (Double)Method.AskUserForInput("Indtast b: ", InputTypes.Double);
            double c = (Double)Method.AskUserForInput("Indtast c: ", InputTypes.Double);
            double D;
            double x;
            double x1;
            double x2;
            double y;
            double y1;
            double y2;



            D = Math.Pow(b, 2) - 4 * a * c;

            Console.WriteLine("Ligningen ser således ud: " +
                "{0} * x\u00b2 + {1} * x + {2}, og diskriminanten er: {3}.",
                a, b, c, D);
            Console.WriteLine("");

            if (a == 0)
            {
                Console.WriteLine("a kan ikke være 0. Prøv igen.");
                Opgave();
            }
            else
            {


                if (D < 0)
                {
                    //Ingen løsning
                    Console.WriteLine("Der er ingen løsning tilgængelig til denne formel.");
                    Console.ReadLine();
                    Program.Menu();
                }
                if (D == 0)
                {
                    //En løsning
                    x = (-b + Math.Sqrt(D)) / (2 * a);
                    y = (Math.Pow(a, 2) * x) + (b * x) + c;

                    Console.WriteLine("Der er 1 løsning til denne ligning.");
                    Console.WriteLine("Punktet for denne løsning ligger på X: {0:n2} og Y: {1:n2}", x, y);
                    Console.ReadLine();
                    Program.Menu();
                }
                if (D > 0)
                {
                    //To løsninger
                    x1 = (-b + Math.Sqrt(D)) / (2 * a);
                    x2 = (-b - Math.Sqrt(D)) / (2 * a);
                    y1 = (Math.Pow(a, 2) * x1) + (b * x1) + c;
                    y2 = (Math.Pow(a, 2) * x2) + (b * x2) + c;

                    Console.WriteLine("Der er 2 løsninger for denne ligning.");
                    Console.WriteLine("Punktet for den ene løsning ligger på X: {0:n2} og Y: {1:n2}", x1, y1);
                    Console.WriteLine("Punktet for den anden løsning ligger på X: {0:n2} og Y: {1:n2}", x2, y2);
                    Console.ReadLine();
                    Program.Menu();
                }

            }

        }
    }
}