﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave4
    {
        static void Opgave4_3()
        {

            Program.current = () => Opgave4_3();
            Console.Clear();
            Console.WriteLine("Opgave 4,3");

            //Asks user for input
            var amount = (double)Method.AskUserForInput("Indtast antal: ", InputTypes.Double);
            var price = (double)Method.AskUserForInput("Indtast pris: ", InputTypes.Double);
            //Call the VAT calculator method
            var sum = (double)VATCalculator(amount,price);

            Console.WriteLine("Total vil blive: {0} DDK",sum);
            Console.ReadLine();
            Intro();

        }

        private static double VATCalculator(double amount, double price)
        {
            //VAT is inverted so it adds 25%
            double VAT = 1.25;
            double salePrice =0;
            double discount = 0;
            double sum = 0;

            //Find the sum without VAT
            salePrice = amount*price;
            //Gives a discount if the price is over 500 DKK
            if (salePrice >= 500)
            {
                //5% discount minus the original price
                discount = 0.95;
                sum = ((salePrice * discount)*VAT);
            }
            else
            {
                sum = salePrice * VAT;
            }
            //Return to main method with the sum price
            return sum;
        }
    }
}