﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave4
    {
        static void Opgave4_1()
        {

            Program.current = () => Opgave4_1();
            Console.Clear();
            Console.WriteLine("Opgave 4,1");
            //Call method to ask for user input         
            var num1 = (int)Method.AskUserForInput("Indtast num1", InputTypes.Int32);
            var num2 = (int)Method.AskUserForInput("Indtast num2", InputTypes.Int32);

            //Compare if num1 is bigger than num2
            if (num1 > num2)
            {
                Console.WriteLine("Største tal er: {0}", num1);
                Console.ReadLine();
            }
            //Compare if num2 is bigger than num1
            else if (num2 > num1)
            {
                Console.WriteLine("Største tal er: {0}", num2);
                Console.ReadLine();
            }
            //Compare if numbers equal each other
            else if (num1 == num2)
            {
                Console.WriteLine("Tallene er lige store!");
                Console.ReadLine();
            }
        }
    }    
}