﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public class Opgave12
    {
        static bool errorCode = true;

        public static void Opgave()
        {

            Intro();
        }
        static void Error()
        {
            if (!errorCode)
            {
                Console.WriteLine("Ugyldtig værdi indtastet. Prøv igen.");
                Console.ReadLine();
                Program.current();
            }
        }

        static void Intro()
        {
            Opgave12 enOpgave = new Opgave12();
            Console.Clear();
            Console.WriteLine("Intro");
            Console.WriteLine("Hvilken opgave vil du gå til?");
            Console.WriteLine("1: Opgave 12,1 \t ");
            Console.WriteLine("2: Opgave 12,2 \t ");
            Console.WriteLine("3: Opgave 12,3 \t ");
            Console.WriteLine("4: Opgave 12,4 \t ");
            Console.WriteLine("5: Opgave 12,5 \t ");
            Console.WriteLine("6: Opgave 12,6 \t ");
            Console.WriteLine("7: Opgave 12,7 \t ");
            Console.WriteLine("8: Opgave 12,8 \t ");
            Console.WriteLine("9: Opgave 12,B \t ");
            Console.WriteLine("Esc: Tilbage til menu");
            Console.WriteLine("Indtast nummer...");

            ConsoleKeyInfo keyPress = Console.ReadKey();

            if (keyPress.Key == ConsoleKey.NumPad1 || keyPress.Key == ConsoleKey.D1)
            {
                enOpgave.Opgave12_1();
            }
            else if (keyPress.Key == ConsoleKey.NumPad2 || keyPress.Key == ConsoleKey.D2)
            {
                enOpgave.Opgave12_2();
            }
            else if (keyPress.Key == ConsoleKey.NumPad3 || keyPress.Key == ConsoleKey.D3)
            {
                enOpgave.Opgave12_3();
            }
            else if (keyPress.Key == ConsoleKey.NumPad4 || keyPress.Key == ConsoleKey.D4)
            {
                enOpgave.Opgave12_4();
            }
            else if (keyPress.Key == ConsoleKey.NumPad5 || keyPress.Key == ConsoleKey.D5)
            {
                enOpgave.Opgave12_5();
            }
            else if (keyPress.Key == ConsoleKey.NumPad6 || keyPress.Key == ConsoleKey.D6)
            {
                enOpgave.Opgave12_6();
            }
            else if (keyPress.Key == ConsoleKey.NumPad7 || keyPress.Key == ConsoleKey.D7)
            {
                enOpgave.Opgave12_7();
            }
            else if (keyPress.Key == ConsoleKey.NumPad8 || keyPress.Key == ConsoleKey.D8)
            {
                enOpgave.Opgave12_8();
            }
            else if (keyPress.Key == ConsoleKey.NumPad9 || keyPress.Key == ConsoleKey.D9)
            {
                enOpgave.Opgave12_B();
            }
            else if (keyPress.Key == ConsoleKey.Escape)
            {
                Program.Menu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Jeg forstod ikke hvad du trykkede. Prøv igen.");
                Console.ReadLine();
                Intro();
            }
        }
        void Opgave12_1()
        {
            Program.current = () => Opgave12_1();
            Console.Clear();
            Console.WriteLine("Opgave 12,1");
            string[] monthNames = new string[12]
            {
                "Januar",
                "Februar",
                "Marts",
                "April",
                "Maj",
                "Juni",
                "Juli",
                "August",
                "September",
                "Oktober",
                "November",
                "December"
            };
            int i = 0;
            foreach (string words in monthNames)
            {
                Console.WriteLine(monthNames[i]);
                i++;
            }

            Console.ReadLine();
            Intro();
        }

        void Opgave12_2()
        {
            Program.current = () => Opgave12_2();
            Console.Clear();
            Console.WriteLine("Opgave 12,2");

            string[] dayNames = new string[7]
            {
                "Mandag",
                "Tirsdag",
                "Onsdag",
                "Torsdag",
                "Fredag",
                "Lørdag",
                "Søndag"
            };
            int i = 0;
            foreach (string words in dayNames)
            {
                Console.WriteLine(dayNames[i]);
                i++;
            }

            Console.ReadLine();
            Intro();
        }
        void Opgave12_3()
        {
            Program.current = () => Opgave12_3();
            Console.Clear();
            Console.WriteLine("Opgave 12,3");


            string[] monthNames = new string[12]
            {
                "Januar",
                "Februar",
                "Marts",
                "April",
                "Maj",
                "Juni",
                "Juli",
                "August",
                "September",
                "Oktober",
                "November",
                "December"
            };
            string[] dayNames = new string[7]
            {
                "Mandag",
                "Tirsdag",
                "Onsdag",
                "Torsdag",
                "Fredag",
                "Lørdag",
                "Søndag"
            };
            OutputArray(monthNames);
            OutputArray(dayNames);

            Console.ReadLine();
            Intro();
        }

        public void OutputArray(string[] input)
        {
            int i = 0;
            foreach (string words in input)
            {
                Console.WriteLine(input[i]);
                i++;
            }
            return;
        }
        void Opgave12_4()
        {
            Program.current = () => Opgave12_4();
            Console.Clear();
            Console.WriteLine("Opgave 12,4");

            int[] testInt = new int[2] { 69, 420 };
            double[] testDouble = new double[2] { 1337, 518008 };
            bool[] testBool = new bool[2] { true, false };

            Console.ReadLine();
            Intro();
        }
        void Opgave12_5()
        {
            Program.current = () => Opgave12_5();
            Console.Clear();
            Console.WriteLine("Opgave 12,5");

            Random r = new Random();
            int[] randomNum = new int[5];
            for (int i = 0; i < randomNum.Length; i++)
            {
                int rNumber = r.Next(101);
                randomNum[i] = rNumber;
            }
            int j = 0;
            foreach (int num in randomNum)
            {
                Console.WriteLine(randomNum[j]);
                j++;
            }

            Console.ReadLine();
            Intro();
        }
        void Opgave12_6()
        {
            Program.current = () => Opgave12_6();
            Console.Clear();
            Console.WriteLine("Opgave 12,6 Onsdagslotto");

            int[] lottoTal = new int[6];
            Random r = new Random();
            for (int i = 0; i < lottoTal.Length; i++)
            {
                lottoTal[i] = r.Next(48);
            }
            Array.Sort(lottoTal);

            Method.OutputArrayInt(lottoTal);

            Console.ReadLine();
            Intro();
        }
        void Opgave12_7()
        {
            Program.current = () => Opgave12_7();
            Console.Clear();
            Console.WriteLine("Opgave 12,7");

            string[] birds = new string[]
            {
                "Musvit",
                "Solsort",
                "Sumpmejse",
                "Gråspurv",
                "Stær",
                "Nattergal"
            };
            Method.OutputArrayString(birds);

            Array.Sort(birds);

            Method.OutputArrayString(birds);

            Console.ReadLine();
            Intro();
        }
        void Opgave12_8()
        {
            Program.current = () => Opgave12_8();
            Console.Clear();
            Console.WriteLine("Opgave 12,8");

            var date = (int)Method.AskUserForInput("Indtast din dato i format 'DDMMYYYY': ", InputTypes.Int32);

            var day = (int)date / 1000000;
            var month = ((int)date / 10000) % 10;
            var year = ((int)date % 10000);
            int dayMax = 0;



            int[] months = new int[12];
            int currentLength = 0;
            for (int m = 0; m < months.Length; m++)
            {
                if (m == 1)
                {
                    if (DateTime.IsLeapYear(year))
                    {
                        dayMax = 29;
                    }
                    else if (!DateTime.IsLeapYear(year))
                    {
                        dayMax = 28;
                    }
                }
                else if (m == 3 || m == 5 || m == 8 || m == 10)
                {
                    dayMax = 30;
                }
                else if (m == 0 || m == 2 || m == 4 || m == 6 || m == 7 || m == 9 || m == 11)
                {
                    dayMax = 31;
                }
                months[m] = currentLength;
                currentLength = currentLength + dayMax;
            }

            Console.WriteLine("Du er født på {0}'s {1} dag!", year, months[month - 1] + day);

            //Method.OutputArrayInt(months);

            Console.ReadLine();
            Intro();
        }

        void Opgave12_B()
        {
            Program.current = () => Opgave12_8();
            Console.Clear();
            Console.WriteLine("Opgave 12,B");

            string[,] skoleSkema = new string[8, 6];
            string[] day = new string[6]
            {
                "Tid\t",
                "Mandag\t",
                "Tirsdag\t",
                "Onsdag\t",
                "Torsdag\t",
                "Fredag\t"
            };
            string[] time = new string[7]
            {
                "8:15-9:00",
                "9:00-9:45",
                "10:00-10:45",
                "10:45-11:30",
                "12:00-12:45",
                "12:45-13:30",
                "14:00-14:45"
            };

            for (int i = 0; i < skoleSkema.GetLength(0); i++)
            {
                for (int j = 0; j < skoleSkema.GetLength(1); j++)
                {
                    if (i == 0)
                        skoleSkema[0, j] = day[j];
                    if (i != 0)
                    {
                        skoleSkema[i, 0] = time[i - 1];

                        if (!(i == 1 || i == 6) && ((j == 1 && i <= 4) || (j == 3 && i <= 5) || (j == 5 && i <= 4)))
                            skoleSkema[i, j] = "prog1\t";
                        else if (!(i == 1 || i == 7) && (j == 2 || j == 4) && (i <= 6))
                            skoleSkema[i, j] = "sys1/db1";
                        else if (j == 1 && (i == 5 || i == 6))
                            skoleSkema[i, j] = "virk1\t";
                        else if ((j == 2 || j == 4) && (i > 6 || i < 8) && i != 1)
                            skoleSkema[i, j] = "Lektiecafe";
                        else
                            skoleSkema[i, j] = "\t";
                    }

                }


            }
            for (int i = 0; i < skoleSkema.GetLength(0); i++)
            {
                Console.WriteLine();
                for (int j = 0; j < skoleSkema.GetLength(1); j++)
                {
                    Console.Write("{0}\t", skoleSkema[i, j]);


                    /*
                    if (i == 0)
                        Console.Write("{0}\t", skoleSkema[i, j]);
                    else if(j==0)
                    {
                        Console.Write("{0}\t", skoleSkema[i,j]);
                    }
                    else Console.Write("{0}\t", j);
                    */
                }
            }

            Console.ReadLine();
            Intro();
        }
    }
}