﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave10
    {

        static void Opgave10_7()
        {
            Program.current = () => Opgave10_7();
            Console.Clear();
            Console.WriteLine("Opgave 10,7");

            Console.WriteLine("Indtast ord til at blive vendt om:");
            string userInput = Console.ReadLine();

            string backwardsWord = TalkBackwards(userInput);


            Console.WriteLine("Dit ord er nu: {0}", backwardsWord);

            Console.ReadLine();
            Intro();
        }

        private static string TalkBackwards(string userInput)
        {
            //Vi laver her vores array om til en returnerbar string
            string[] backwardsWord = BackwardsTalk(userInput);
            return string.Join("", backwardsWord);
        }

        private static string[] BackwardsTalk(string userInput)
        {
            int letterLength = userInput.Length;
            int currentLength = letterLength;

            //Starter array til at samle vores ord
            string[] array = new string[letterLength];

            foreach (char letters in userInput)
            {
                //Sætter hvert bogstav til sidst
                currentLength--;
                array[currentLength] = letters.ToString();
            }
            return array;
        }
    }
}