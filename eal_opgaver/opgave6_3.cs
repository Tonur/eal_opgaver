﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave6
    {
        static void Opgave6_3()
        {

            Program.current = () => Opgave6_3();
            Console.Clear();
            Console.WriteLine("Opgave 6,3");

            Console.WriteLine("Indtast landekode: ");
            string countryCode = Console.ReadLine();

            switch (countryCode)
            {
                case "DK":
                    Console.WriteLine("Der tales dansk i dette land.");
                    Console.ReadLine();
                    Intro();
                    break;
                case "UK":
                case "AU":
                case "IE":
                    Console.WriteLine("Der tales engelsk i dette land.");
                    Console.ReadLine();
                    Intro();
                    break;
                case "SE":
                    Console.WriteLine("Der tales svensk i dette land.");
                    Console.ReadLine();
                    Intro();
                    break;
                case "PL":
                    Console.WriteLine("Der tales polsk i dette land.");
                    Console.ReadLine();
                    Intro();
                    break;
                case "NL":
                    Console.WriteLine("Der tales nederlandsk/hollandsk i dette land.");
                    Console.ReadLine();
                    Intro();
                    break;
                default:
                    Console.WriteLine("Dit indput var ikke gyldigt. Prøv igen...");
                    Console.ReadLine();
                    Opgave6_3();
                    break;
                
            }
        }        
    }
}