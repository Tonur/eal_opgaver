﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class BruteForce
    {

        public static void Opgave()
        {
            Program.current = () => Opgave();
            Console.Clear();
            Console.WriteLine("Opgave Bruteforce");

            long number = 1050504368559379;

            for (long i = 1; i <= (1050504368559379 / 2); i++)
            {
                if (number % i == 0)
                {
                    Console.WriteLine("Tallet {0} går op i heltallet.",i);
                }
            }

            Console.ReadLine();
            Program.Menu();
        }
    }
}
            