﻿using System;
namespace eal_opgaver
{
    public class Opgave1
    {

        //Tal
        public static sbyte sbyteTal = sbyte.MaxValue;
        public static short shortTal = short.MaxValue;
        public static int intTal = Int32.MaxValue;
        public static long longTal = long.MaxValue;
        public static byte byteTal = byte.MaxValue;
        public static ushort ushortTal = ushort.MaxValue;
        public static uint uintTal = UInt32.MaxValue;
        public static ulong ulongTal = ulong.MaxValue;

        //Symboler
        public static char charSymbol = char.MaxValue;

        //Float tal
        public static float floatTal = float.MaxValue;
        public static double doubleTal = double.MaxValue;

        //Decimal tal
        public static decimal decimalTal = decimal.MaxValue;

        //Bool sand/falsk
        public static bool boolStatement = true;


        public static void Opgave()
        {
            Console.Clear();

            Console.WriteLine("sbyte max " + sbyteTal);

            sbyteTal++;
            Console.WriteLine("sbyte overflow " + sbyteTal);

            Console.WriteLine("short max " + shortTal);
            shortTal++;
            Console.WriteLine("short overflow " + shortTal);

            Console.WriteLine("int max " + intTal);
            shortTal++;
            Console.WriteLine("int overflow " + intTal);

            Console.WriteLine("long max " + longTal);
            longTal++;
            Console.WriteLine("long overflow " + longTal);

            Console.WriteLine("byte max " + byteTal);
            byteTal++;
            Console.WriteLine("byte overflow " + byteTal);

            Console.WriteLine("ushort max " + ushortTal);
            ushortTal++;
            Console.WriteLine("ushort overflow " + ushortTal);

            Console.WriteLine("uint max " + uintTal);
            uintTal++;
            Console.WriteLine("uint overflow " + uintTal);

            Console.WriteLine("ulong max " + ulongTal);
            ulongTal++;
            Console.WriteLine("ulong overflow " + ulongTal);

            Console.WriteLine("char max " + charSymbol);
            charSymbol++;
            Console.WriteLine("char overflow " + charSymbol);

            Console.WriteLine("float max " + floatTal);
            floatTal++;
            Console.WriteLine("float overflow " + floatTal);

            Console.WriteLine("double max " + doubleTal);
            doubleTal++;
            Console.WriteLine("double overflow " + doubleTal);
            
            Console.WriteLine("decimal max " + decimalTal);
            //decimalTal++;
            //Console.WriteLine("decimal overflow " + decimalTal);
            
            Console.WriteLine("Decimal lavede fejl så jeg blev nødt til at fjerne den...");

            Console.WriteLine("bool max " + boolStatement);

            Console.WriteLine("bool overflow " + boolStatement);

            Console.ReadLine();
            return;
        }

    }
}