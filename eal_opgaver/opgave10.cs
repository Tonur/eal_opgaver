﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave10
    {
        static bool errorCode = true;       

        public static void Opgave()
        {

            Intro();
        }
            static void Error()
        {
            if (!errorCode)
            {
                Console.WriteLine("Ugyldtig værdi indtastet. Prøv igen.");
                Console.ReadLine();
                Program.current();
            }
        }

        static void Intro()
        {
            Console.Clear();
            Console.WriteLine("Intro");
            Console.WriteLine("Hvilken opgave vil du gå til?");
            Console.WriteLine("1: Opgave 10,1 \t Metodekald med inputs");
            Console.WriteLine("2: Opgave 10,2 \t To be or not to be - Statisk");
            Console.WriteLine("3: Opgave 10,3 \t Metode om skudår");
            Console.WriteLine("4: Opgave 10,4 \t Metode om gyldig dato");
            Console.WriteLine("5: Opgave 10,5 \t Metode om foreach tæl a");
            Console.WriteLine("6: Opgave 10,6 \t Metode om foreach tæl udvidet");
            Console.WriteLine("6: Opgave 10,7 \t Dameskib");
            Console.WriteLine("7: Tilbage til menu");
            Console.WriteLine("Indtast nummer...");

            ConsoleKeyInfo keyPress = Console.ReadKey();

            if (keyPress.Key == ConsoleKey.NumPad1 || keyPress.Key == ConsoleKey.D1)
            {
                Opgave10_1();
            }
            else if (keyPress.Key == ConsoleKey.NumPad2 || keyPress.Key == ConsoleKey.D2)
            {
                Opgave10_2();
            }
            else if (keyPress.Key == ConsoleKey.NumPad3 || keyPress.Key == ConsoleKey.D3)
            {
                Opgave10_3();
            }
            else if (keyPress.Key == ConsoleKey.NumPad4 || keyPress.Key == ConsoleKey.D4)
            {
                Opgave10_4();
            }
            else if (keyPress.Key == ConsoleKey.NumPad5 || keyPress.Key == ConsoleKey.D5)
            {
                Opgave10_5();
            }
            else if (keyPress.Key == ConsoleKey.NumPad6 || keyPress.Key == ConsoleKey.D6)
            {
                Opgave10_6();
            }
            else if (keyPress.Key == ConsoleKey.NumPad7 || keyPress.Key == ConsoleKey.D7)
            {
                Opgave10_7();
            }
            else if (keyPress.Key == ConsoleKey.NumPad8 || keyPress.Key == ConsoleKey.D8)
            {
                Program.Menu();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Jeg forstod ikke hvad du trykkede. Prøv igen.");
                Console.ReadLine();
                Intro();
            }
        }
    }
}