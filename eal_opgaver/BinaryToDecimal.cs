﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class BinaryToDecimal
    {
        static public void Opgave()
        {
            Program.current = () => Opgave();
            Console.Clear();
            Console.WriteLine("Opgave Binær til Decimal");
            BinaryToDecimal DecimalConverter = new BinaryToDecimal();
            int input = (int)Method.AskUserForInput("Indtast binær tal: "
                , InputTypes.Int32);

            int result = DecimalConverter.ToDecimal(input);
            Console.WriteLine("Resultat er: {0}", result);


            Console.ReadLine();
            Program.Menu();
        }
        /* public string ToDecimal(int input)
         {
             string container = "";

             while (input > 0)
             {

             }

             return container;
         }*/
        public int ToDecimal(int input)
        {
            int i = 0;
            int num = 0;
            while (input > 0)
            {
                num = num + ((input % 10) * Convert.ToInt32(Math.Pow(2, i)));
                input = (input/10);
                i++;
            }
            return num;
        }
    }
}