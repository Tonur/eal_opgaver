﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave10
    {
               
        static void Opgave10_6()
        {
            Program.current = () => Opgave10_6();
            Console.Clear();
            Console.WriteLine("Opgave 10,6");

            Console.WriteLine("Indtast en streng");
            string userInput = Console.ReadLine();
            Console.WriteLine("Hvilket bogstav skal der chekkes for?: ");
            char letter = Convert.ToChar(Console.ReadLine());



            int letterNumber = CheckYoLetter(userInput, letter);
            Console.WriteLine("Der er {0} {1}'er i din tekst.", letterNumber,letter);

            Console.ReadLine();
            Intro();
        }

        private static int CheckYoLetter(string userInput, char letter)
        {
            int count = 0;

            foreach (char x in userInput)
            {
                if (x == letter)
                {
                    count++;
                }               
            }
            return count;

        }
    }    
}