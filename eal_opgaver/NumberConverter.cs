﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class NumberConverter
    {
        public static char NumSys = 'A';
        static public void Opgave()
        {

            Program.current = () => Opgave();
            Console.Clear();
            Console.WriteLine("Opgave Talsystem Konverter");

            var value = Intro();
            if(NumSys == 'A')
                Console.WriteLine("Dit nye tal: {0}", value);
            else if(NumSys == 'B')
                Console.WriteLine("Dit nye tal: {0:o}", value);
            else if(NumSys == 'C')
                Console.WriteLine("Dit nye tal: {0:x}", value);

            Console.ReadLine();
            Program.Menu();
        }
        static string Intro()
        {
            NumberConverter NumConvert = new NumberConverter();
            Console.Clear();
            Console.WriteLine("Intro");
            Console.WriteLine("Hvilket talsystem vil du konvertere?");
            Console.WriteLine("1: Decimal til binær");
            Console.WriteLine("2: Binær til decimal");
            Console.WriteLine("3: Decimal til Octal");
            Console.WriteLine("4: Octal til Decimal");
            Console.WriteLine("5: Decimal til Hexadecimal");
            Console.WriteLine("6: Hexadecimal til Decimal");
            Console.WriteLine("7: Tilbage til menu");
            Console.WriteLine("Indtast nummer...");

            ConsoleKeyInfo keyPress = Console.ReadKey();

            int input;
            string returnValue = "";


            if (keyPress.Key == ConsoleKey.NumPad1 || keyPress.Key == ConsoleKey.D1)
            {
                input = (int)Method.AskUserForInput("Indtast heltal som konverteres til binær: ", InputTypes.Int32);
                returnValue = NumConvert.ToBinary(input);
            }
            else if (keyPress.Key == ConsoleKey.NumPad2 || keyPress.Key == ConsoleKey.D2)
            {
                NumSys = 'A';
                input = (int)Method.AskUserForInput("Indtast binært tal som konverteres til decimal: ", InputTypes.Int32);
                returnValue = Convert.ToString(NumConvert.ToBDecimal(input));
            }
            else if (keyPress.Key == ConsoleKey.NumPad3 || keyPress.Key == ConsoleKey.D3)
            {
                NumSys = 'B';
                input = (int)Method.AskUserForInput("Indtast heltal som konverteres til octal: ", InputTypes.Int32);
                returnValue = NumConvert.ToOctal(input);
            }
            else if (keyPress.Key == ConsoleKey.NumPad4 || keyPress.Key == ConsoleKey.D4)
            {
                NumSys = 'A';
                input = (int)Method.AskUserForInput("Indtast octal som konverteres til decimal: ", InputTypes.Int32);
                returnValue = Convert.ToString(NumConvert.ToODecimal(input));
            }
            else if (keyPress.Key == ConsoleKey.NumPad5 || keyPress.Key == ConsoleKey.D5)
            {
                NumSys = 'C';
                input = (int)Method.AskUserForInput("Indtast Decimal som konverteres til Hexadecimal: ", InputTypes.Int32);
                returnValue = NumConvert.ToHexadecimal(input);
            }
            else if (keyPress.Key == ConsoleKey.NumPad6 || keyPress.Key == ConsoleKey.D6)
            {
                NumSys = 'A';
                input = (int)Method.AskUserForInput("Indtast Hexadecimal som konverteres til Decimal: ", InputTypes.Int32);
                returnValue = Convert.ToString(NumConvert.ToHDecimal(input));
            }
            else if (keyPress.Key == ConsoleKey.NumPad7 || keyPress.Key == ConsoleKey.D7)
            {
                Intro();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Jeg forstod ikke hvad du trykkede. Prøv igen.");
                Console.ReadLine();
                Intro();
            }
            return returnValue;
        }


        public string ToBinary(int input)
        {
            string container = "";
            int rem;
            while (input > 0)
            {
                rem = input % 2;
                input = input / 2;
                container = (Convert.ToString(rem)) + container;
            }
            return container;
        }
        public int ToBDecimal(int input)
        {
            int i = 0;
            int num = 0;
            while (input > 0)
            {
                num = num + ((input % 10) * Convert.ToInt32(Math.Pow(2, i)));
                input = (input / 10);
                i++;
            }
            return num;
        }
        public string ToOctal(int input)
        {
            int rem = 0;
            string container = "";

            while (input > 0)
            {
                rem = input % 8;
                input = input / 8;
                container = (Convert.ToString(rem)) + container;
            }
            return container;
        }
        public int ToODecimal(int input)
        {
            int i = 0;
            int num = 0;
            while (input > 0)
            {
                num = num + ((input % 10) * Convert.ToInt32(Math.Pow(8, i)));
                input = (input / 10);
                i++;
            }
            return num;
        }
        public string ToHexadecimal(int input)
        {
            int rem = 0;
            string container = "";

            while (input > 0)
            {
                rem = input % 16;
                input = input / 16;
                container = (Convert.ToString(rem)) + container;
            }
            return container;
        }
        public int ToHDecimal(int input)
        {
            int i = 0;
            int num = 0;
            while (input > 0)
            {
                num = num + ((input % 10) * Convert.ToInt32(Math.Pow(16, i)));
                input = (input / 10);
                i++;
            }
            return num;
        }

    }
}