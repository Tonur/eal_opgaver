﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave10
    {
               
        static void Opgave10_3()
        {
            Program.current = () => Opgave10_3();
            Console.Clear();
            Console.WriteLine("Opgave 10,3");

            int year = (int)Method.AskUserForInput("Indtast år for at tjekke om det er skudår", 
                InputTypes.Int32);
            bool leapYear = LeapYear(year);

            if (leapYear)
            {
                Console.WriteLine("Det er et skudår!");
            }
            else if (!leapYear)
            {
                Console.WriteLine("Det er ikke et skudår...");
            }

            Console.ReadLine();
            Intro();
        }

        static bool LeapYear(int year)
        {
            bool leap;

            if (DateTime.IsLeapYear(year))
            {
                leap = true;
            }
            else
            {
                leap = false;
            }

            return leap;
        }
    }    
}