﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave7
    {
        static void Opgave7_6()
        {

            Program.current = () => Opgave7_6();
            Console.Clear();
            Console.WriteLine("Opgave 7,6");
            int jn = 0;

            for (int i = 1; i < 11; i++)
            {
                for (int j = 1; j < 11; j++)
                {
                    jn = j * i;
                    Console.Write("{0} \t", jn);
                }
                Console.WriteLine();
                Console.WriteLine();
            }

            Console.ReadLine();
            Intro();
        }

    }
}