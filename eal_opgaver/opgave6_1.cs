﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave6
    {
        static void Opgave6_1()
        {

            Program.current = () => Opgave6_1();
            Console.Clear();
            Console.WriteLine("Opgave 6,1");
            //Call method to ask for user input       
            Console.WriteLine("Indtast bog nummer:");
            string bookNum = Console.ReadLine();
            int bookCode = 0;
            string category = "";
            char bookCode1 = bookNum[0];
            char bookCode2 = bookNum[1];

            bookCode = ((int)Char.GetNumericValue(bookCode1) * 10) + (int)Char.GetNumericValue(bookCode2);


            if (bookCode == 00 || bookCode <= 09)
            {
                category = "Blandede skrifter";
            }
            else if (bookCode <= 19)
            {
                category = "Filosofi, Psykologi";
            }
            else if (bookCode <= 29)
            {
                category = "Religion";
            }
            else if (bookCode <= 39)
            {
                category = "Samfundsvidenskab, Pædagogik";
            }
            else if (bookCode <= 49)
            {
                category = "Geografi og rejser";
            }
            else if (bookCode <= 59)
            {
                category = "Naturvidenskab og matematik";
            }
            else if (bookCode <= 69)
            {
                category = "Praktiske fag";
            }
            else if (bookCode <= 79)
            {
                category = "Kunst, teater, film, musik, spil, idræt";
            }
            else if (bookCode <= 89)
            {
                category = "Litteratur, sprog";
            }
            else if (bookCode <= 99)
            {
                category = "Historie";
            }


            Console.WriteLine("Bogen tilhørere kategorien: {0}.",category);
            Console.ReadLine();


            Intro();
        }

    }
}