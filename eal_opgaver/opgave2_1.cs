﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave2
    {
               
        static void Opgave2_1()
        {
            Program.current = () => Opgave2_1();
            Console.Clear();
            Console.WriteLine("Opgave 2,1");

            //Input gets converted into double

            var width = (double)Method.AskUserForInput("Indtast bredde i cm: ", InputTypes.Double);

            var length = (double)Method.AskUserForInput("Indtast længde i cm: ", InputTypes.Double);

            var height = (double)Method.AskUserForInput("Indtast højde i cm: ", InputTypes.Double);

            //Calculate  surface area
            var surface = (width * length + width * height + length * height) * 2;

            Console.WriteLine("Bredden\t\t{0} cm", width);
            Console.WriteLine("Længden\t\t{0} cm", length);
            Console.WriteLine("Højden\t\t{0} cm", height);
            Console.WriteLine("Overfladen\t{0} cm\u00b2", surface);
            Console.ReadLine();
            Intro();
        }
    }    
}