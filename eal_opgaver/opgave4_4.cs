﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave4
    {
        //We use type double as it is a price calculation that might return a decimal
        const double HOURSALERY = 120;
        const double BONUS = 1.75;
        static void Opgave4_4()
        {

            Program.current = () => Opgave4_4();
            Console.Clear();
            Console.WriteLine("Opgave 4,4");

            
            //Asks user for input
            double weeklyHourCount = (double)Method.AskUserForInput("" +
                "Indtast time antal for denne uge:", InputTypes.Double);
            double weeklyDeduction = (double)Method.AskUserForInput("" +
                "Indtast dit skattefradrag(Normalt 46.000 kr., efterlad tom for den normale værdi):",
                InputTypes.Double)
                /12/4;//Divide by 12 and 4 to get weekly deduction
            double deductionPercent = (double)Method.AskUserForInput("" +
                "Indtast din trækprocent(Normalt 39%, efterlad tom for den normale værdi):",
                InputTypes.Double); 
            //Statements if the deduction questions were left empty
            if (weeklyDeduction == 0)
            {
                weeklyDeduction = 46000/12/4;
            }
            if (deductionPercent != 0)
            {
                //Get the percentage to subtract
                deductionPercent = (100 - deductionPercent)/100;
            }
            if (deductionPercent == 0)
            {
                //Set the default percentage to substract to 39%
                deductionPercent = 0.61;
            }
            
            //Call the Salery Calculator method
            double sumAfterTAX = SaleryCalculator(
                HOURSALERY, weeklyHourCount, weeklyDeduction, deductionPercent);

            Console.WriteLine("Din løn efter skat er: {0} kr.",sumAfterTAX);
            Console.ReadLine();
            Intro();
        }

        private static double SaleryCalculator
            (double HOURSALERY, double weeklyHourCount,
            double weeklyDeduction, double deductionPercent)
        {
            double sumBeforeTAX;
            if (weeklyHourCount > 37)
            {
                //Detract the overtime from the weekly hours worked
                double overTime = weeklyHourCount - 37;
                //Aplly 75% extra to the overtime and calculate sum before TAX
                sumBeforeTAX = HOURSALERY * weeklyHourCount + ((HOURSALERY * overTime) * BONUS);
            }
            else
            {
                sumBeforeTAX = HOURSALERY * weeklyHourCount;
            }
            //Monthly Deduction split into weekly
            var sumWithDeduction =sumBeforeTAX-(weeklyDeduction); 
            var sumAfterTAX = (sumWithDeduction*deductionPercent)+weeklyDeduction;
            return sumAfterTAX;
            throw new NotImplementedException();
        }
    }    
}