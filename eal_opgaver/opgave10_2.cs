﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave10
    {
        static void Opgave10_2()
        {
            Program.current = () => Opgave10_2();
            Console.Clear();
            Console.WriteLine("Opgave 10,1");

            Console.WriteLine("Indtast to tal for at " +
                "sammenlige hvilket er størst");
            int num1 = (int)Method.AskUserForInput("Indtast tal 1: ", InputTypes.Int32);
            int num2 = (int)Method.AskUserForInput("Indtast tal 2: ", InputTypes.Int32);

            //var bigNum = Compareinator2(num1, num2); Kan ikke finde

            Console.WriteLine("Det største tal er: {0}.", 1/*bigNum*/);

            Console.ReadLine();
            Intro();
        }

        int Compareinator2(int a, int b)
        {
            int bigNum = 0;
            if (a > b)
            {
                bigNum = a;
            }
            else if (b > a)
            {
                bigNum = b;
            }
            else if (a == b)
            {
                bigNum = a;
            }
            return bigNum;
        }

    }
}