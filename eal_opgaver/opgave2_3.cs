﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave2
    {

        static void Opgave2_3()
        {
            Program.current = () => Opgave2_3();
            Console.Clear();
            Console.WriteLine("Opgave 2,3");

            Console.WriteLine("Konverter Fahrenheit til Celsius.");
            Console.Write("Indtast Fahrenheit grader: ");
            var fTemp = (double)Method.AskUserForInput("Indtast Fahrenheit grader: ", InputTypes.Double);
            var cTemp = (fTemp - 32) / 1.80;

            Console.WriteLine("Celsius temperatur: {0:n2} \t\t Fahrenheit temperatur: {1:n2}", cTemp, fTemp);
            Console.ReadLine();
            Intro();
        }
    }    
}