﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave6
    {
        static void Opgave6_2()
        {

            Program.current = () => Opgave6_2();
            Console.Clear();
            Console.WriteLine("Opgave 6,2");
            var month = (int)Method.AskUserForInput("Indtast månedsnummer: ", InputTypes.Int32);

            switch (month)
            {
                case 1:
                    Console.WriteLine("Januar");
                    break;
                case 2:
                    Console.WriteLine("Februar");
                    break;
                case 3:
                    Console.WriteLine("Marts");
                    break;
                case 4:
                    Console.WriteLine("April");
                    break;
                case 5:
                    Console.WriteLine("Maj");
                    break;
                case 6:
                    Console.WriteLine("Juni");
                    break;
                case 7:
                    Console.WriteLine("Juli");
                    break;
                case 8:
                    Console.WriteLine("August");
                    break;
                case 9:
                    Console.WriteLine("September");
                    break;
                case 10:
                    Console.WriteLine("Oktober");
                    break;
                case 11:
                    Console.WriteLine("November");
                    break;
                case 12:
                    Console.WriteLine("December");
                    break;
                default:
                    Console.WriteLine("Ugyldig måned. Intast et korrekt månedstal...");
                    Console.ReadLine();
                    Opgave6_2();
                    break;
            }
            Console.ReadLine();
            Intro();

        }
    }    
}