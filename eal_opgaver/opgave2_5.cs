﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave2
    {
        static void Opgave2_5()
        {
            Program.current = () => Opgave2_5();
            Console.Clear();
            Console.WriteLine("Opgave 2,5");

            Console.WriteLine("Dato Operation");
            var currentDate = (double)Method.AskUserForInput("Indtast din dato i format 'DDMMYYYY': ", InputTypes.Double);
            var year = (int)currentDate % 10000;
            var month = ((int)currentDate / 10000) % 100;
            var day = ((int)currentDate / 1000000);
            int dayMax = 0;

            //Leapyear Check
            if (month > 12)
            {
                Console.WriteLine("Du har indtastet en ugyldig dato. Prøv igen.");
                Console.ReadLine();
                Opgave2_5();
            }
            //Leapyear Check

            if (DateTime.IsLeapYear(year))
            {
                dayMax = 29;
            }
            if (!DateTime.IsLeapYear(year))
            {
                dayMax = 28;
            }
            if (month == 4 || month == 6 || month == 9 || month == 11)
            {
                dayMax = 30;
            }
            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
            {
                dayMax = 31;
            }
            if (day > dayMax)
            {
                Console.WriteLine("Du har indtastet en ugyldig dato. Prøv igen.");
                Console.ReadLine();
                Opgave2_5();
            }

            Console.WriteLine("Dag: {0} \t Måned: {1} \t År: {2}", day, month, year);
            Console.ReadLine();

            Intro();
        }
    }
}