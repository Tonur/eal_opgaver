﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eal_opgaver;
namespace methods
{
    public static class Method
    {
        public static object AskUserForInput(string inputString, InputTypes inputType)
        {
            Console.WriteLine();
            Console.Write(inputString);
            switch (inputType)
            {
                case InputTypes.Double:
                    return CheckForDoubleInput();
                case InputTypes.Int32:
                    return CheckForInt32Input();
                case InputTypes.Long:
                    return CheckForLongInput();
                default:
                    break;
            }
            return null;
        }

        private static double CheckForDoubleInput()
        {
            double output;
            string testString = Console.ReadLine();
            if (testString == "")
            {
                output = 0;
                return output;
            }
            var errorCode = double.TryParse(testString, out output);
            Error(errorCode);
            return output;
        }
        private static int CheckForInt32Input()
        {
            int output;
            string testString = Console.ReadLine();
            if (testString == "")
            {
                output = 0;
                return output;
            }
            var errorCode = int.TryParse(testString, out output);
            Error(errorCode);
            return output;
        }
        private static long CheckForLongInput()
        {
            long output;
            string testString = Console.ReadLine();
            if(testString == "")
            {
                output = 0;
                return output;
            }
            var errorCode = long.TryParse(testString, out output);
            Error(errorCode);
            return output;
        }
        public static void OutputArrayString(string[] input)
        {
            int i = 0;
            foreach (string words in input)
            {
                Console.WriteLine(input[i]);
                i++;
            }
            return;
        }
        public static void OutputArrayInt(int[] input)
        {
            int i = 0;
            foreach (int num in input)
            {
                Console.WriteLine(input[i]);
                i++;
            }
            return;
        }

        static void Error(bool errorCode)
        {
            if (!errorCode)
            {
                Console.WriteLine("Ugyldtig værdi indtastet. Prøv igen.");
                Console.ReadLine();
                Program.current();
            }
        }
        public static int CountDigits(int d1, int noDigits)
        {
            /*
             * CountDigits gets a number and counts the rows of numbers
             * I.E. 4342 will return noDigits 4, because it has 4 rows of numbers
             * Initialize with d1 as the number and noDigits as 0 for the start value
             */
            if (d1 == 0)
            {
                return noDigits;
            }
            return CountDigits(d1 / 10, ++noDigits);
        }
    }

    public enum InputTypes
    {
        Double,
        Int32,
        Long,
        
    }
}

