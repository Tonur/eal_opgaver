﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave4
    {
        static void Opgave4_2()
        {

            Program.current = () => Opgave4_2();
            Console.Clear();
            Console.WriteLine("Opgave 4,2");
            double postage = 0;

            Console.WriteLine("Beregn pris for brev.");
            //Asks user for input
            var weight = (int)Method.AskUserForInput
                ("Indtast weight i gram, der rundes op til nærmeste kategori: ",
                InputTypes.Int32);
            
            //Compare if prices are bigger, smaller or equal to the catagorized prices
            //Then change the postage to the specified amount
            if (weight <= 100)
            {
                postage = 9.00;
            }
            else if(weight >= 100)
            {
                postage = 18.00;
            }
            else if (weight >= 250)
            {
                postage = 36.00;
            }
            else if (weight >= 500)
            {
                postage = 45.00;
            }
            else if (weight >= 1000)
            {
                postage = 54.00;
            }
            //If weight is over 2000 it is not possible to post the letter
            //Inform the user with an error message
            else if (weight > 2000)
            {
                Console.WriteLine("Det er desværre ikke muligt at poste et brev så tungt.");
                Console.WriteLine("Prøv igen og indtast en ny weight.");
                Console.ReadLine();
                Opgave4_2();
            }

            Console.WriteLine("Brev kommer til at koste {0} DKK at sende.", postage);
            Console.ReadLine();
            Intro();

        }
    }    
}