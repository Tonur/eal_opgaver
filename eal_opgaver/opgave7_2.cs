﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave7
    {
        static void Opgave7_2()
        {
            Program.current = () => Opgave7_2();
            Console.Clear();
            Console.WriteLine("Opgave 7,2");
            double sum = 0;
            double sumAvg = 0;

            for (int i = 0; i <= 25; i++)
            {
                sum = i + sum;
                sumAvg = sum / i;
            }
            
            Console.WriteLine("Sum af tal er: {0}," +
                " og gennemsnittet af dette er: {1}.",sum, sumAvg);
            Console.ReadLine();
            Intro();
        }

    }
}