﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class randomNumber
    {
        static public void Opgave()
        {

            Program.current = () => Opgave();
            Console.Clear();
            Console.WriteLine("Opgave Random number");


            Random r = new Random();
            int rnumber = r.Next(101);
            Console.WriteLine("Der er blevet genereret et tilfældigt " +
                "tal mellem 1 og 100. Gæt hvad det er.");


            lead:
            Console.WriteLine();
            Console.WriteLine("Vælg en ledetråd:");
            Console.WriteLine("1.\t Indtast tal, du får derefter at vide " +
                "om det tilfældige tal er større eller lavere");
            Console.WriteLine("2.\t Få at vide om tallet er lige eller ulige.");
            Console.WriteLine("3.\t Få at vide om tallet er over eller under 50.");
            Console.WriteLine();
            ConsoleKeyInfo keyPress = Console.ReadKey();

            if (keyPress.Key == ConsoleKey.NumPad1 || keyPress.Key == ConsoleKey.D1)
            {
                int guess = (int)Method.AskUserForInput("Indtast dit gæt...:", InputTypes.Int32);
                if(guess > rnumber)
                {
                    Console.WriteLine("Dit gæt er større end det tilfældige tal.");
                }
                else if (guess < rnumber)
                {
                    Console.WriteLine("Dit gæt er mindre end det tilfældige tal.");
                }
                else if (guess == rnumber)
                {
                    Console.WriteLine("Dit gæt er det tilfældige tal! Huraah!");
                    goto sucess;
                }
                goto lead;

            }
            else if (keyPress.Key == ConsoleKey.NumPad2 || keyPress.Key == ConsoleKey.D2)
            {
                if(rnumber % 2 == 0)
                {
                    Console.WriteLine("Dit nummer er lige");
                }
                if (!(rnumber % 2 == 0))
                {
                    Console.WriteLine("Dit nummer er ulige");
                    
                }
                goto lead;
            }
            else if (keyPress.Key == ConsoleKey.NumPad3 || keyPress.Key == ConsoleKey.D3)
            {
                if (rnumber >= 50)
                {
                    Console.WriteLine("Det tilfældige tal er større end 50.");
                }
                else if (rnumber <= 50)
                {
                    Console.WriteLine("Det tilfældige tal er mindre end 50.");
                }
                goto lead;
            }
            sucess:



            Console.ReadLine();
            Program.Menu();
        }

    }
}