﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Factoriality
    {

        public static void Opgave()
        {
            Program.current = () => Opgave();
            Console.Clear();
            Console.WriteLine("Opgave Factorial");

            ulong num = 5;


            ulong res = Factorial(num);

            Console.WriteLine("Factorial af {0}! er {1}.",num, res);

            Console.ReadLine();
            Program.Menu();
        }

        private static ulong Factorial(ulong num)
        {
            ulong res = 1;
            if (num == 0)
            {
                res = 1;
            }
            else
            {
                res = num * Factorial(num - 1);
            }
            return res;
        }
    }
}
            