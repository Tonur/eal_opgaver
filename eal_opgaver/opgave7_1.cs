﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using methods;
namespace eal_opgaver
{
    public partial class Opgave7
    {
        static void Opgave7_1()
        {
            Program.current = () => Opgave7_1();
            Console.Clear();
            Console.WriteLine("Opgave 7,1");

            for(int i = 0; i < 11; i++)
            {
                Console.WriteLine(i*i);
            }

            Console.ReadLine();
            Intro();
        }

    }
}